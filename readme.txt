=== WooCommerce Elavon Converge Gateway ===
Author: skyverge
Tags: woocommerce
Requires at least: 4.4
Tested up to: 4.9.2

Adds the Elavon Converge (Virtual Merchant) Gateway to your WooCommerce website. Requires an SSL certificate.

See http://docs.woothemes.com/document/elavon-vm-payment-gateway/ for full documentation.

== Installation ==

1. Upload the entire 'woocommerce-gateway-elavon' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
